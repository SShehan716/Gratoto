// Export pages
export '/pages/splash/splash_widget.dart' show SplashWidget;
export '/pages/result_page/result_page_widget.dart' show ResultPageWidget;
export '/pages/image_test/image_test_widget.dart' show ImageTestWidget;
export '/pages/login_page/login_page_widget.dart' show LoginPageWidget;
export '/pages/loading_pag/loading_pag_widget.dart' show LoadingPagWidget;
export '/pages/sig_up_page/sig_up_page_widget.dart' show SigUpPageWidget;
export '/pages/forgot_password/forgot_password_widget.dart'
    show ForgotPasswordWidget;
export '/pages/who_we_are/who_we_are_widget.dart' show WhoWeAreWidget;
export '/home/home_widget.dart' show HomeWidget;
